<?php
?>
<div id="widget-generator">
<div class="intro">
<h1><span>Create a Widget</span><span class="intro">Display the latest Digg news on your site by adding a Digg Widget, now with more choices for what to show and how it's displayed. When news is updated on Digg, it will automatically be displayed on your site, in the format that you choose. <strong>To get started, follow the steps below to customize your widget.</strong></span></h1>
</div>
<div class="widget-preview">
<div id="widget-preview">
<div class="style-options">
<h4>Select a type</h4>
<div id="layout-select">
<div id="layout1" class="layout on">
<div class="single"></div>
Single<br />Column
</div>
<div id="layout2" class="layout">
<div class="double"></div>
Double<br />Columns
</div>
<br />
</div>
<form class="widget-generator-form">
<h4>Change appearance</h4>
<div><label class="content-option">Title:</label> <input type="text" name="widget-title" value="CBSNews.com on Digg" /></div>
<div class="color"><label>Header background</label><input type="text" name="hdrBg" /></div>
<div class="show-more-style-options">More display options</div>
<div class="more-style-options">
<div class="color"><label>Header text</label><input type="text" name="hdrTxt" /></div>
<div class="color"><label>Content background</label><input type="text" name="bdyBg" /></div>
<div class="color"><label>Horizontal rule</label><input type="text" name="stryBrdr" /></div>
<div class="color"><label>Links</label><input type="text" name="lnk" /></div>
<div id="tab-colors">
<div class="color"><label>Tab background</label><input type="text" name="tabBg" /></div>
<div class="color"><label>Tab text</label><input type="text" name="tabTxt" /></div>
<div class="color"><label>Selected tab text</label><input type="text" name="tabOnTxt" /></div>
</div>
<div id="subhd-color" class="color"><label>Column heading</label><input type="text" name="subHd" /></div>
<div id="description-color" class="color"><label>Story description text</label><input type="text" name="descTxt" /></div>
<div class="dimensions">Width: <input type="text" name="widget-width" value="300" size="3" /> Height: <input type="text" name="widget-height" value="" size="3" /></div>
<div class="content-option"><input type="checkbox" name="descriptions" id="show-descriptions" /> <label for="show-descriptions">Show story descriptions</label></div>
<div class="content-option"><input type="checkbox" name="widget-header" id="show-header" checked="checked" /> <label for="show-header">Show header</label></div>
<div class="content-option"><input type="checkbox" name="widget-footer" id="show-footer" checked="checked" /> <label for="show-footer">Show footer</label></div>
<div class="content-option"><input type="checkbox" name="widget-diggs" id="show-digg-counts" checked="checked" /> <label for="show-digg-counts">Show diggs</label></div>
<div class="content-option"><input type="checkbox" name="widget-thumb" id="show-thumbs" checked="checked" /> <label for="show-thumbs">Show thumbnails</label></div>
<div class="content-option"><input type="checkbox" name="widget-rounded" id="show-rounded" checked="checked" /> <label for="show-rounded">Round corners (Mozilla and Webkit)</label></div>
<div class="content-option"><input type="checkbox" name="widget-stylesheet" id="use-stylesheet" checked="checked" /> <label for="use-stylesheet">Use default stylesheet</label> (<a id="view-stylesheet" href="http://widgets.digg.com/css/widgets.css" target="_blank">view</a>)</div>
<div class="content-option"><input type="checkbox" name="widget-targ" id="link-targ" /> <label for="link-targ">Open links in a new window</label></div>
</div>
</form>
</div>
<div class="preview-hint">
<h4>Preview</h4> Your Widget
</div>
<div class="widget-container">
<div id="widget-container">
<div id="digg-widget"></div>
</div>
</div>
<br />
</div>
</div>
<div id="what-to-show">
<div class="intro">
<h4>Choose what to show</h4>
<span>Select from the following ways to populate your widget.</span>
</div>
<div id="tab-selector">
<h4 id="single-tab">Single Tab</h4>
<form class="widget-generator-form">
<button class="add-tab" type="button">Add a Tab</button>
<p>Or add more stories in <a id="add-column" href="javascript://">another column</a>.</p>
</form>
</div>
<div id="fields-container">
<form class="widget-generator-form" id="method-form">
<div class="fld selected"> <input type="radio" class="r" name="news_type" value="domain" checked="checked" /> <select name="source-poporup">
<option value="popular">Popular</option>
<option value="upcoming">Upcoming</option>
<option value="all">All</option>
</select> <label >stories from the source site</label> <input type="text" name="url" size="17" value="CBSNews.com" />
<div class="ffld">
<input type="radio" class="r" /> <select name="url-sort">
<option value="promote_date-desc">sorted by date</option>
<option value="digg_count-desc">sorted by diggs</option>
</select> in the
<select name="mindate">
<option value="">last millenium</option>
<option value="1">last 24 hours</option>
<option value="7">last 7 days</option>
<option value="30">last 30 days</option>
<option value="365">last 365 days</option>
</select> <input type="checkbox" name="fallback" checked="checked" /> Use fallback content if too few stories are found
</div>
</div>
<div class="fld"><input type="radio" class="r" name="news_type" value="front" />
<label for="news-type1">All popular stories in</label>&nbsp;<select name="news_front">
<option value="" selected="selected">All Topics</option>
<option name="apple" value="apple">Apple</option>
<option name="design" value="design">Design</option>
<option name="gadgets" value="gadgets">Gadgets</option>
<option name="hardware" value="hardware">Hardware</option>
<option name="tech_news" value="tech_news">Industry News</option>
<option name="linux_unix" value="linux_unix">Linux/Unix</option>
<option name="microsoft" value="microsoft">Microsoft</option>
<option name="mods" value="mods">Mods</option>
<option name="programming" value="programming">Programming</option>
<option name="security" value="security">Security</option>
<option name="software" value="software">Software</option>
<option name="business_finance" value="business_finance">Business & Finance</option>
<option name="world_news" value="world_news">World News</option>
<option name="politics" value="politics">Political News</option>
<option name="political_opinion" value="political_opinion">Political Opinion</option>
<option name="celebrity" value="celebrity">Celebrity</option>
<option name="movies" value="movies">Movies</option>
<option name="music" value="music">Music</option>
<option name="television" value="television">Television</option>
<option name="comics_animation" value="comics_animation">Comics & Animation</option>
<option name="gaming_news" value="gaming_news">Industry News</option>
<option name="pc_games" value="pc_games">PC Games</option>
<option name="playable_web_games" value="playable_web_games">Playable Web Games</option>
<option name="nintendo" value="nintendo">Nintendo</option>
<option name="playstation" value="playstation">PlayStation</option>
<option name="xbox" value="xbox">Xbox</option>
<option name="baseball" value="baseball">Baseball</option>
<option name="environment" value="environment">Environment</option>
<option name="general_sciences" value="general_sciences">General Sciences</option>
<option name="basketball" value="basketball">Basketball</option>
<option name="extreme_sports" value="extreme_sports">Extreme</option>
<option name="space" value="space">Space</option>
<option name="football" value="football">Football - US/Canada</option>
<option name="golf" value="golf">Golf</option>
<option name="hockey" value="hockey">Hockey</option>
<option name="motorsport" value="motorsport">Motorsport</option>
<option name="olympics" value="olympics">Olympics</option>
<option name="soccer" value="soccer">Soccer</option>
<option name="tennis" value="tennis">Tennis</option>
<option name="other_sports" value="other_sports">Other Sports</option>
<option name="arts_culture" value="arts_culture">Arts & Culture</option>
<option name="autos" value="autos">Autos</option>
<option name="educational" value="educational">Educational</option>
<option name="food_drink" value="food_drink">Food & Drink</option>
<option name="health" value="health">Health</option>
<option name="travel_places" value="travel_places">Travel & Places</option>
<option name="comedy" value="comedy">Comedy</option>
<option name="odd_stuff" value="odd_stuff">Odd Stuff</option>
<option name="people" value="people">People</option>
<option name="pets_animals" value="pets_animals">Pets & Animals</option>
</select>
</div>
<div class="fld">
<input type="radio" class="r" name="news_type" value="top10"/> <label for="news-type0">Top 10 list from</label> <select name="news_top">
<option value="" selected="selected">All Topics</option>
<option value="technology">Technology</option>
<option value="science">Science</option>
<option value="world_business">World &amp; Business</option>
<option value="sports">Sports</option>
<option value="entertainment">Entertainment</option>
<option value="gaming">Gaming</option>
<option value="lifestyle">Lifestyle</option>
<option value="offbeat">Offbeat</option>
<option value="" title="media=news">News</option>
<option value="" title="media=videos">Videos</option>
<option value="" title="media=images">Images</option>
</select>
</div>
<div class="fld">
<input type="radio" class="r" name="news_type" value="user" /> <label for="news-type3">Stories </label> <select name="news_user" >
<option value="dugg">dugg</option>
<option value="submissions">submitted</option>
</select> <label for="username">by user</label> <input type="text" name="username" value="dtrinh" size="17" />
</div>
<div class="fld"> <input type="radio" class="r" name="news_type" value="search" /> <label>Search results for</label> <input type="text" name="apisearch" size="17" value="Lady Gaga" /> in <select name="search-topics">
<option value="" selected="selected">All Topics</option>
<option value="technology">Technology</option>
<option value="science">Science</option>
<option value="world_business">World &amp; Business</option>
<option value="sports">Sports</option>
<option value="entertainment">Entertainment</option>
<option value="gaming">Gaming</option>
<option value="lifestyle">Lifestyle</option>
<option value="offbeat">Offbeat</option>
</select>
<select name="search-sort">
<option value="promote_date-desc">sorted by date</option>
<option value="digg_count-desc">sorted by diggs</option>
</select>
</div>
<div class="fld f"> <input type="radio" class="r" id="news-type6" name="news_type" value="friends" /> <label>Stories your friends have</label> <select name="news_friends">
<option value="dugg">dugg</option>
<option value="submissions">submitted</option>
<option value="commented">commented on</option>
</select> <span id="yourusername">Your username: <input type="text" name="myusername" value="dtrinh" /></span>
</div>
<div class="storycount">
Number of Items
<input name="count" value="5" size=2" maxlength="2" />
</div>
</form>
</div>
<br />
</div>
<div class="finished">
<strong>Finished? Then your widget is ready to go!</strong>
</div>
</div>

<script type="text/javascript" language="JavaScript" src="http://widgets.digg.com/widgets.js"></script>
