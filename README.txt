
-- SUMMARY --

DiggABLE

This module provides an easy framework to add Digg buttons and Digg widgets to
your page.  Digg buttons help drive traffic to your content by allowing easy
Digging and submission into the Digg ecosystem.  Digg widgets allow you to
supplement your content with the top stories from Digg either through the lens
of your site or through a specific area or topic from Digg.

Out of the box, it allows you to:

  1.  Place yellow smart Digg buttons in the three size options:
      compact, medium and large
  2.  Generate a customized Digg widget with different placement options

The yellow smart Digg buttons can tell if a web page has been Dugg or not and
displays the proper number count.

The widget generator can show the most Dugg content by domain, topic, search
term or even show a particular user's Dugg stories.  After selecting the type
of content to display, it's easy to save that state and decide where to
position it on a page.

For a full description of the module, visit the project page:
  http://drupal.org/project/diggable

To submit bug reports and feature suggestions, or to track changes:
  http://drupal.org/project/issues/diggable


-- REQUIREMENTS --

None.


-- INSTALLATION --

* Install as usual, see http://drupal.org/node/70151 for further information.


-- CONFIGURATION --

* Enable and customize the Digg Button at Administer >> Site configuration >>
  DiggABLE >> Digg Button.
  
* Enable and customize the Digg Widget at Administer >> Site configuration >>
  DiggABLE >> Digg Widget.

* After enabling the Digg Widget, visit the Blocks Configuration page at
  Administer >> Site building >> Blocks to enable the Digg Widget block and
  place it in a region of your current theme.


-- TROUBLESHOOTING --

* If the Digg Widget does not display after being enabled and placing the block
  in a region, go to Administer >> Site building >> Blocks and click on the
  "configure" link next to the Digg Widget block.  Save the resulting settings
  page (it is not necessary to change any settings).
